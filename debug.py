# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Siamese Network
# 

import random

import numpy as np
# %%
## libraries
import pandas as pd
import tensorflow as tf
from keras.datasets import mnist
from sklearn import utils
from tensorflow.keras import backend as K
from tensorflow.keras.layers import (Conv2D, Dense, Flatten, Input, Lambda,
                                     MaxPooling2D)
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import RMSprop
from tensorflow.python.keras.utils.vis_utils import plot_model

from utils import *

# %% [markdown]
# # load the datasets of the numbers 4 and 9

# %%
(train_X, train_y), (test_X, test_y) = mnist.load_data()
train_X = train_X.astype('float32')
train_y = train_y.astype('float32')
dataset4 = np.array([train_X[i] for i in range(len(train_y)) if train_y[i] == 4])
dataset4 = dataset4.astype('float32')
# shape = (5842, 28, 28)
dataset9 = np.array([train_X[i]for i in range(len(train_y)) if train_y[i] == 9])
dataset9 = dataset9.astype('float32')


data41,data42 = sliceByPercent(dataset4,0.5)
#shape = (2921, 28, 28)
data411,data412 = sliceByPercent(data41,0.5)
data91,data92 = sliceByPercent(dataset9,0.5)
data911,data912 = sliceByPercent(data91,0.5)

# %% [markdown]
# ## Create paired and unpaired numbers

# %%
# same numbers
length4 = min(len(data411),len(data412))
length9 = min(len(data911),len(data912))

paired1 = [np.array([data411[i],data412[i]]) for i in range(length4)]
paired1 = np.array(paired1)
#shape = (1460, 2, 28, 28)
paired2 = [np.array([data911[i],data912[i]]) for i in range(length9)]
paired2 = np.array(paired2)
paired = np.concatenate((paired1,paired2))
#shape = (2946, 2, 28, 28)
print(paired.shape)

# unpaired
length = min(len(data42),len(data92))
unpaired = [np.array([data42[i],data92[i]]) for i in range(length)]
unpaired = np.array(unpaired)
#shape = (2920, 2, 28, 28)
print(unpaired.shape)

# %% [markdown]
# ## Add labels to the paired and unpaired

# %%
y_paired = np.ones((paired.shape[0],1))
y_unpaired = np.zeros((unpaired.shape[0],1))

# structure of paired and unpoaired :
# [[imgi1,imgi2]]
this_img = 10
paired[this_img].shape
## or in np.array mode
###paired[i,:] = 


plotCouple(paired[this_img],'Coupled pairs')
plotCouple(unpaired[this_img],'Uncoupled pairs')

# %% [markdown]
# # Create the siamese model
# %% [markdown]
# ## Function G_w model

# %%
def initializeModel():
    input = Input(shape=(28,28,1))
    C1 = Conv2D(15,6)(input)
    S2 = MaxPooling2D((2,2))(C1)
    C3 = Conv2D(30,11)(S2)
    F3 = Flatten()(C3)
    F3= Dense(2)(F3)
    return Model(inputs = input,outputs = F3)

# %% [markdown]
# ## initialize model and plot

# %%
G_w = initializeModel()
plot_model(G_w, show_shapes=True, show_layer_names=True)


# %%
## help functions

def euclideanDistance(vects):
    x, y = vects
    sum_square = K.sum(K.square(x - y), axis=1, keepdims=True)
    return K.sqrt(K.maximum(sum_square, K.epsilon()))

def eucl_dist_output_shape(shapes):
    shape1, shape2 = shapes
    return (shape1[0], 1)


# %%
# create the 2 branches
inputLeft = Input(shape=(28,28,1),name='input_left')
output_left = G_w(inputLeft)

inputRight = Input(shape=(28,28,1),name='input_right')
output_right = G_w(inputRight)

#measure the Euclidean distance between the outputs
output = Lambda(euclideanDistance, name="output_layer",
output_shape=eucl_dist_output_shape)([output_left,output_right])

# specify the inputs and output of the model
model = Model([inputLeft, inputRight], output)

# plot model graph
plot_model(model, show_shapes=True, show_layer_names=True, to_file='outer-model.png')

# %% [markdown]
# # Contrastetive loss

# %%
def ContrastiveLossWithMargin(margin):
    def ContrastiveLoss(y_true,y_pred):
        L_s = 0.5 * K.square(y_pred)
        print('Ls === ',L_s)
        L_d = 0.5 * K.square(K.maximum(0.,margin - y_pred))
        return K.mean((1-y_true) * L_s + y_true * L_d)
    return ContrastiveLoss



# %%
data = np.concatenate((paired,unpaired))
y = np.concatenate((y_paired,y_unpaired))
data,y = utils.shuffle(data, y)

# create test,val and train set
data_train, data_test = sliceByPercent(data,0.8)
data_train, data_val = sliceByPercent(data_train,0.8)

y_train, y_test = sliceByPercent(y,0.8)
y_train, y_val = sliceByPercent(y_train,0.8)
y_train = K.constant(y_train)
y_test = K.constant(y_test)
y_val = K.constant(y_val)

print(data_train[:,0].shape[0])

data_train1 = K.constant(reshapeValues(data_train[:,0]))
data_train2 = K.constant(reshapeValues(data_train[:,1]))
data_val1 = K.constant(reshapeValues(data_val[:,0]))
data_val2 = K.constant(reshapeValues(data_val[:,1]))
# data_test = reshapeValues(data_test)



# train the model
rms = RMSprop()
model.compile(loss=ContrastiveLossWithMargin(1), optimizer=rms)
history = model.fit([data_train1,data_train2], y_train, epochs=30,
 batch_size =128, validation_data=([data_val1,data_val2],y_val))


