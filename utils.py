import math
import matplotlib.pyplot as plt
from tensorflow.keras import backend as K

def sliceByPercent(listToSlice,p):
    '''
    Slice list with a percentage. p must be between 0 and 1
    '''
    n = math.floor(p*len(listToSlice))
    return listToSlice[0:n], listToSlice[n+1:]

def show_image(image):
    '''
    Plot an image
    '''
    plt.figure()
    plt.imshow(image)
    plt.colorbar()
    plt.grid(False)
    plt.show()

def plotCouple(X,title='') :
    '''
    Plot a couple of images horizontally.
    X must be an array of 2 images
    '''
    x1 = X[0]
    x2 = X[1]

    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    ax1.imshow(x1)
    ax1.set_title(title)
    ax2.imshow(x2)

a=2

def reshapeValues(v) :
    s=v.shape
    return v.reshape(s[0],s[1],s[2],1)